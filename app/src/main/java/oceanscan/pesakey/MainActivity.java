package oceanscan.pesakey;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.hover.sdk.main.HoverParameters;
import com.hover.sdk.onboarding.HoverIntegrationActivity;

public class MainActivity extends AppCompatActivity {
    public static int INTEGRATE_REQUEST = 1;
    public static int HOVER_CHECK_BALANCE = 3;
    public static int HOVER_REQUEST_MONEY=4;
    private static String TAG = MainActivity.class.getSimpleName();

    String[] mpesaMenu = {"Send Money", "Pay Bill", "Lipa Na MPESA", "Receive Money", "Check Balance"};
    ListView menu;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        menu = (ListView) findViewById(R.id.menu);


        Intent integrationIntent = new Intent(this, HoverIntegrationActivity.class);
        integrationIntent.putExtra(HoverIntegrationActivity.SERVICE_IDS, new int[]{8});
        startActivityForResult(integrationIntent, INTEGRATE_REQUEST);


        menu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    startActivity(new Intent(getApplicationContext(), SendMoney.class));
                } else if (position == 1) {
//                    Intent i = new HoverParameters.Builder(this).request("pay_bill", amount, currency).extra("businessNumber", value).extra("accountNumber", value).from(8).buildIntent();
//                    startActivityForResult(i, HOVER_REQUEST);
                } else if (position == 2) {
//                    Intent i = new HoverParameters.Builder(this).request("pay_merchant", amount, currency).extra("businessNumber", value).from(8).buildIntent();
//                    startActivityForResult(i, HOVER_REQUEST);
                } else if (position == 3) {
                    Intent i = new HoverParameters.Builder(MainActivity.this).request("receive").from(8).buildIntent();
                    startActivityForResult(i, HOVER_REQUEST_MONEY);
                } else if (position == 4) {
                    Intent i = new HoverParameters.Builder(MainActivity.this).request("check_balance").from(8).buildIntent();
                    startActivityForResult(i, HOVER_CHECK_BALANCE);
                }
            }
        });


    }
    // https://www.usehover.com/sdk/transactions

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        Log.i(TAG,"Result "+data.getDataString().toString());
        if (requestCode == INTEGRATE_REQUEST && resultCode == RESULT_CANCELED) {
            Toast.makeText(this, "Error: " + data.getStringExtra("error"), Toast.LENGTH_LONG).show();
        } else if (requestCode == INTEGRATE_REQUEST && resultCode == RESULT_OK) {
            menu.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, mpesaMenu));
            Toast.makeText(this, "Success! ServiceId: " + data.getIntExtra("serviceId", -1) + ", Service Name: " + data.getStringExtra("serviceName") + ", Currency: " + data.getStringExtra("currency") + ", Country Name: " + data.getStringExtra("countryName") + ", Operator: " + data.getStringExtra("opSlug"), Toast.LENGTH_LONG).show();
        }
    }
}
