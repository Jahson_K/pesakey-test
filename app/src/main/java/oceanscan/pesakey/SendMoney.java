package oceanscan.pesakey;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hover.sdk.main.HoverParameters;

public class SendMoney extends AppCompatActivity {
    EditText amount, phoneNumber;
    Button sendMoney;


    public static int HOVER_REQUEST = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_money);

        amount = (EditText) findViewById(R.id.amount);
        phoneNumber = (EditText) findViewById(R.id.phone_number);
        sendMoney = (Button) findViewById(R.id.send_money);

        sendMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(phoneNumber.getText().toString())) {
                    phoneNumber.setError("Phone number is required");
                } else if (TextUtils.isEmpty(amount.getText().toString())) {
                    amount.setError("Amount is required");
                } else {
                    Intent i = new HoverParameters.Builder(SendMoney.this)
                            .request("send_money", amount.getText().toString(), "KES", phoneNumber.getText().toString()).from(8).buildIntent();
                    startActivityForResult(i, HOVER_REQUEST);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        Log.i(TAG,"Result "+data.getDataString().toString());
        if (requestCode == HOVER_REQUEST && resultCode == RESULT_CANCELED) {
            Toast.makeText(this, "Error: " + data.getStringExtra("error"), Toast.LENGTH_LONG).show();
        } else if (requestCode == HOVER_REQUEST && resultCode == RESULT_OK) {
            Toast.makeText(this, "Success! Transaction completed. ", Toast.LENGTH_LONG).show();
        }
    }
}
